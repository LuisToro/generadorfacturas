package controllers;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import entities.Factura;
import repositories.PersistencePDF;
import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;
import useCases.IPersistence;
import useCases.Persistence;

import static spark.Spark.*;

public class Main {
	public static IPersistence persistence;
	static File file = new File("file.json");
	static JSONParser parser = new JSONParser();
	static Factura factura;
	
	public static void main(String[] args) {
		port(4567);
		try {
			post("/datosFactura", (request, response) -> {
            	Map<String, Object> model = new HashMap<>();
            	saveDataIntoJson(request.queryParams("numeroTelefonico"), request.queryParams("mes"));
            	
            	extractData();
            	persistence= new Persistence(new PersistencePDF(factura));
                persistInPDF();
                
            	model.put("factura", factura );
            	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/factura.vm"));
            });
			
            get("/facturar", (request, response) -> 
            {
            	Map<String, Object> model = new HashMap<>();
            	model.put("factura", factura );
            	return new VelocityTemplateEngine().render(new ModelAndView(model, "velocity/factura.vm"));
            });
            
        

        } catch (Exception e) {
            System.out.println("Exception in NetClientGet:- " + e);
        }
    }

	private static void persistInPDF() {
		persistence.persistInPDF();
	}

	private static void saveDataIntoJson(String numeroTelefonico, String mes) throws MalformedURLException, IOException, ProtocolException {
		String output="";
		
		URL url = new URL("http://localhost:8080/costoLlamadaCliente/"+ numeroTelefonico + "/mes/" + mes);//your url i.e fetch data from .
		System.out.println(url);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.setRequestProperty("Accept", "application/json");
		
		if (connection.getResponseCode() != 200) {
		    throw new RuntimeException("Failed : HTTP Error code : "
		            + connection.getResponseCode());
		}
		
		InputStreamReader in = new InputStreamReader(connection.getInputStream());
		BufferedReader reader = new BufferedReader(in);
		FileWriter fileWriter=new FileWriter(file);
		BufferedWriter writer = new BufferedWriter(fileWriter);

		while ((output = reader.readLine()) != null) {
		    System.out.println(output);
		    writer.write(output);
		}
		
		writer.flush();
		fileWriter.close();
		writer.close();
		connection.disconnect();
	}
	
	private static void saveDataIntoObject(JSONObject cdr) 
    {
        //Get employee first name
        long numeroTelefonico = (long) cdr.get("numeroTelefonico");    
        System.out.println(numeroTelefonico);
        String mes = (String) cdr.get("mes");    
        System.out.println(mes);
        double montoMes = (double) cdr.get("montoMes");    
        System.out.println(montoMes);
        
        factura = new Factura(numeroTelefonico,mes,montoMes);
    }
	
	private static void extractData() 
    {
		try {
			Object obj = parser.parse(new FileReader("file.json"));
			JSONObject jsonFile = (JSONObject) obj;
			saveDataIntoObject(jsonFile);
	        
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
    }
}
	
