package useCases;
public class Persistence implements IPersistence {
	private IPersistencePDF persistencePDF;
	
	public Persistence(IPersistencePDF persistencePDF) {
		this.persistencePDF=persistencePDF;
	}
	
	@Override
	public void persistInPDF() {
		persistencePDF.createPDF();
	}

}
