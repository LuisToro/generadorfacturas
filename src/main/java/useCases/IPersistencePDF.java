package useCases;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;

public interface IPersistencePDF {
	void createPDF();
	void addMetaData(Document document);
	void addContent(Document document) throws DocumentException;
	void createTable(Paragraph paragraph) throws BadElementException;
	void addEmptyLine(Paragraph paragraph, int number);
}
