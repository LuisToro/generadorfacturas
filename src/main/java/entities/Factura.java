package entities;

public class Factura {
	private long numeroTelefonico;
	private double montoMes; 
	private String mes;
	
	public Factura(long numeroTelefonico,String mes,double montoMes){
		this.numeroTelefonico = numeroTelefonico;
		this.mes=mes;
		this.montoMes = montoMes;
	}

	public long getNumeroTelefonico() {
		return numeroTelefonico;
	}

	public void setNumeroTelefonico(Integer numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}

	public double getMontoMes() {
		return montoMes;
	}

	public void setMontoMes(double montoMes) {
		this.montoMes = montoMes;
	}

	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

}
